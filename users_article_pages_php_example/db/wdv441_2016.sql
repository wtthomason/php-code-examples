-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 12, 2016 at 06:14 PM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `wdv441_2016`
--

-- --------------------------------------------------------

--
-- Table structure for table `news_articles`
--

CREATE TABLE `news_articles` (
  `article_id` int(11) NOT NULL,
  `article_title` varchar(150) NOT NULL,
  `article_content` text NOT NULL,
  `article_author` varchar(50) NOT NULL,
  `article_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `news_articles`
--

INSERT INTO `news_articles` (`article_id`, `article_title`, `article_content`, `article_author`, `article_date`) VALUES
(1, 'my first article', 'This is my first article yay!!!!', 'WT', '2016-02-18 19:00:00'),
(2, 'Test Article 2', '                content 2            ', 'wt', '2016-02-17 17:47:52'),
(3, 'Test Article 2', 'content 2', 'GG', '2016-02-17 17:47:55'),
(4, 'Test Article 2', 'content 2', 'GG', '2016-02-18 17:31:06'),
(5, 'Test Article 2', 'content 2', 'GG', '2016-02-18 19:02:30'),
(6, 'testint', '                alkjlksd                                        ', 'test validation', '1023-10-24 00:00:00'),
(7, 'Test Article 2', 'content 2', 'GG', '2016-02-20 00:06:47'),
(8, 'Test Article 2', 'content 2', 'GG', '2016-02-20 00:13:12'),
(9, 'Test Article 2', 'content 2', 'GG', '2016-02-20 00:24:13'),
(10, 'Test Article 2', 'content 2', 'GG', '2016-02-20 00:57:50'),
(11, 'Test Article 2', 'content 2', 'GG', '2016-03-27 04:02:50'),
(12, 'Test Article 2', 'content 2', 'GG', '2016-03-27 04:02:49'),
(13, 'Test Article 2', 'content 2', 'GG', '2016-03-27 04:02:53'),
(14, 'Test Article 2', 'content 2', 'GG', '2016-03-27 04:03:40');

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `page_id` int(11) NOT NULL,
  `page_title` varchar(200) NOT NULL,
  `meta_tags` varchar(200) NOT NULL,
  `h1` varchar(200) NOT NULL,
  `content` text NOT NULL,
  `banner_image` varchar(100) NOT NULL,
  `url_key` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`page_id`, `page_title`, `meta_tags`, `h1`, `content`, `banner_image`, `url_key`) VALUES
(1, 'gg test 1a', '', '', '', '', ''),
(2, 'GG test 2', '', '', '', '', ''),
(3, 'tyson', '', '', '', '', ''),
(4, 'Page 1', 'Page1', 'This is Page 1', 'Hello, this is my page 1 testing now 1234. I hope this works great.', '', 'Page one'),
(5, 'Page 2', 'page2', 'Page 2', '<p style="text-align: center;">&nbsp;</p>\r\n<h1 style="text-align: left;">Hello Page 2</h1>\r\n<p style="text-align: center;">This is my page two. I hope that this works out well and I do not have to redue all of this content please wrap so that I do not have to type any more. Thanks &nbsp; &nbsp;</p>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<li>one</li>\r\n<li>two</li>\r\n<li>three</li>\r\n<li>four&nbsp;</li>\r\n</ul>', '', 'page2');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `user_level` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `username`, `password`, `user_level`) VALUES
(1, 'admin', 'admin', 'Administrator'),
(2, 'tyson', 'tyson', 'Administrator'),
(3, 'New Guy', 'admin', 'Contributor'),
(4, 'new2', 'admin', 'Contributor'),
(5, 'new', 'admin', 'Moderator'),
(6, 'testing', 'admin', 'Guest'),
(7, 'testing again', 'hello', 'admin'),
(8, 'hello ', 'admin', 'Contributor'),
(9, 'what', 'hello', 'Guest'),
(10, 'new3', 'admin', 'Contributor'),
(11, 'new4', 'admin', 'Contributor'),
(12, 'new5', 'admin', 'Contributor'),
(13, 'new6', 'admin', 'Contributor'),
(14, 'admin2', 'admin', 'Administrator');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `news_articles`
--
ALTER TABLE `news_articles`
  ADD PRIMARY KEY (`article_id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`page_id`),
  ADD KEY `url_key` (`url_key`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `news_articles`
--
ALTER TABLE `news_articles`
  MODIFY `article_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `page_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
