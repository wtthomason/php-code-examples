<?php
// to edit article 1: news_edit.php?article_id=1
// to create: news_edit.php

require_once('../inc/newsArticle.class.php');

session_start();
if ( empty ($_SESSION['user_id'])) {
    exit(header("Location: user_login.php"));
}

$searchValue = null;
if (isset($_GET['search']))
{
    $searchValue = filter_var($_GET['search'], FILTER_SANITIZE_STRING);
}

$newsArticles = new newsArticle();
$newsList = $newsArticles->getArticles($searchValue);

$columnsToDisplay = 
array(
    "Author" => "article_author",
    "Article Title" => "article_title",
    "Article Date" => "article_date",
);


include_once('../tpl/article_list.tpl.php');


?>
