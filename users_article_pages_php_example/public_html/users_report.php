<?php
require_once('../inc/users.class.php');

if (isset($_GET['btnCancel'])) 
{
    header("location: users_report.php");
    exit;
}

session_start();
if ( empty ($_SESSION['user_id'])) {
    exit(header("Location: user_login.php"));
}

$users = new users();

$users->checkUserLevel( $_SESSION['user_id']);

if (isset($_GET['download']) && $_GET['download'] == "yes")
{
    $users->downloadUserReport($_GET);
    exit;
}

if (isset($_GET['btnSubmit']))
{
    unset($_GET['btnSubmit']);
    
    $reportData = $users->getUserReportData($_GET);
}

$columnsToDisplay =
    array(
    "Username" => "username",
    "User Level" => "user_level",
);

include_once('../tpl/users_report.tpl.php');
?>