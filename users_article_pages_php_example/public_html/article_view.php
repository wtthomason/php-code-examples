<?php
require_once('../inc/newsArticle.class.php');

session_start();
if ( empty ($_SESSION['user_id'])) {
    exit(header("Location: user_login.php"));
}

$newsArticle = new newsArticle();

if (isset($_GET['article_id']) && $_GET['article_id'] >0)
{
    $newsArticle->load($_GET['article_id']);
} 

$dataValues = $newsArticle->data;

include_once('../tpl/article_view.tpl.php');
?>