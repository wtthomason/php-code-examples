<?php
require_once('../inc/users.class.php');
session_start();
$users = new users();
$errors = array();
$success = "";

if (isset($_POST['btnSubmit'])) {
    if (empty($_POST['username']) || empty($_POST['password'])) {
    $error = "Username or Password is invalid";
    }
    
    unset($_POST['btnSubmit']);
    if($users->login($_POST)){
        exit(header("Location: users_list.php"));
    }
}


include_once('../tpl/user_login.tpl.php')
?>


