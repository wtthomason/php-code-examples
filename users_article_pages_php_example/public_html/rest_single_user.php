<?php

require_once('../inc/users.class.php');

$users = new users();

if (isset($_GET['user_id']) && $_GET['user_id'] >0)
{
    $users->load($_GET['user_id']);
} 

$dataValues = $users->data;

echo json_encode($dataValues);

?>