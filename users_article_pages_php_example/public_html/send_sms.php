<?php
// to edit article 1: news_edit.php?article_id=1
// to create: news_edit.php

require_once('../inc/sms.class.php');

session_start();
if ( empty ($_SESSION['user_id'])) {
    exit(header("Location: user_login.php"));
} 

$sms = new sms();
$errors = array();

if (isset($_POST['btnCancel']))
{
    header("location:users_list.php");
    exit;
}

if (isset($_POST['btnSubmit']))
{
    unset($_POST['btnSubmit']);
    $sms->set($_POST);
    if ($sms->validate($_POST, $errors)) 
    {
        if ($sms->sendSms()) 
        {
            header("location:sms_sent.php");
            exit;
        }
    }
}

$dataValues = $sms->data;

include_once('../tpl/send_sms.tpl.php');
?>