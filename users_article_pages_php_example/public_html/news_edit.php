<?php
// to edit article 1: news_edit.php?article_id=1
// to create: news_edit.php

require_once('../inc/newsArticle.class.php');

session_start();
if ( empty ($_SESSION['user_id'])) {
    exit(header("Location: user_login.php"));
} 

$newsArticle = new newsArticle();
$errors = array();

if (isset($_GET['article_id']) && $_GET['article_id'] >0)
{
    $newsArticle->load($_GET['article_id']);
} 
elseif (isset($_POST['article_id']) && $_POST['article_id'] >0) 
{
    $newsArticle->load($_POST['article_id']);
}

if (isset($_POST['btnSubmit']))
{
    unset($_POST['btnSubmit']);
    $newsArticle->set($_POST);
    if ($newsArticle->validate($_POST, $errors)) 
    {
        if ($newsArticle->save()) 
        {
            header("location:article_save.php");
            exit;
        }
    }
}

$dataValues = $newsArticle->data;

include_once('../tpl/news_edit.tpl.php');
?>
