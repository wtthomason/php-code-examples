<?php
require_once('../inc/pages.class.php');

session_start();
if ( empty ($_SESSION['user_id'])) {
    exit(header("Location: user_login.php"));
}

if (isset($_POST['btnCancel'])) 
{
    header("location: page_list.php");
    exit;
}

$pages = new pages();
$errors = array();

if (isset($_GET['page_id']) && $_GET['page_id'] >0)
{
    $pages->load($_GET['page_id']);
} 
elseif (isset($_POST['page_id']) && $_POST['page_id'] >0) 
{
    $pages->load($_POST['page_id']);
}

if (isset($_POST['btnSubmit']))
{
    unset($_POST['btnSubmit']);
    $pages->set($_POST);
    if ($pages->validate()) 
    {
        if ($pages->save()) 
        {
            $pages->saveUploadedImage($_FILES);
            header("location:page_save.php");
            exit;
        }
    }
}

$dataValues = $pages->data;

include_once('../tpl/page_edit.tpl.php');
?>