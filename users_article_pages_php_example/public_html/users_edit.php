<?php
// to edit article 1: news_edit.php?article_id=1
// to create: news_edit.php

require_once('../inc/users.class.php');

if (isset($_POST['btnCancel']))
{
    header("location:users_list.php");
    exit;
}

$users = new users();
$errors = array();


if (isset($_GET['user_id']) && $_GET['user_id'] >0)
{
    $users->load($_GET['user_id']);
} 
elseif (isset($_POST['user_id']) && $_POST['user_id'] >0) 
{
    $users->load($_POST['user_id']);
}

if (isset($_POST['btnSubmit']))
{
    unset($_POST['btnSubmit']);
    $users->set($_POST);
    if ($users->validate($_POST, $errors)) 
    {
        if ($users->save()) 
        {
            $users->saveUploadedImage($_FILES);
            header("location:save_user.php");
            exit;
        }
    }
}



$dataValues = $users->data;

include_once('../tpl/users_edit.tpl.php')

?>
