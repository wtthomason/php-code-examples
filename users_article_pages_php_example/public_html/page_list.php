<?php
require_once('../inc/pages.class.php');

$searchValue = null;
if (isset($_GET['search']))
{
    $searchValue = filter_var($_GET['search'], FILTER_SANITIZE_STRING);
}

$pages = new pages();

$pageList = $pages->getList($searchValue);
//$newsListArray = $newsArticles->getListArray();

$columnsToDisplay = 
array(
    "Page Title" => "page_title",
    "H1" => "h1",
    "URL Key" => "url_key",
);

include_once('../tpl/page_list.tpl.php');
?>