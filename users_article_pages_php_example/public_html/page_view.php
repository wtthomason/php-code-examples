<?php
session_start();
require_once('../inc/pages.class.php');

$pages = new pages();

// ------------------------------------------------------------
// update this block of code to use the loadByURLKey function
if (isset($_GET['page']) && $_GET['page']!="")
{
    $pages->loadByURLKey($_GET['page']);
}
// ------------------------------------------------------------

$dataValues = $pages->data;

include_once('../tpl/page_view.tpl.php');
?>