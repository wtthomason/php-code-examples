<html>
   <head>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">

        <!-- jQuery library -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>

        <!-- Latest compiled JavaScript -->
   </head>
    <body>
       <div class="container">
       <br>
       <a href="users_list.php" class="btn btn-default"> Back to User List</a>
        <form action="users_report.php" method="get">
            <div>
                <div><h3>Filter</h3></div>
                <div>
                    User Name: <input type="text" name="username_filter" value="<?php echo (isset($_GET['username_filter']) ? $_GET['username_filter'] : ''); ?>"/><br><br>
                    User Level:
                    <select name="user_level_filter" id="user_level_filter">
                         <option value="">Please select one</option>
                            <option  value="Guest" <?php echo (isset($_GET['user_level_filter']) && $_GET['user_level_filter'] == "Guest" ? "selected" : ""); ?>>Guest</option>
                            <option  value="Contributor" <?php echo (isset($_GET['user_level_filter']) && $_GET['user_level_filter'] == "Contributor" ? "selected" : ""); ?>>Contributor</option>
                            <option value="Moderator" <?php echo (isset($_GET['user_level_filter']) && $_GET['user_level_filter'] == "Moderator" ? "selected" : ""); ?>>Moderator</option>
                            <option value="Administrator" <?php echo (isset($_GET['user_level_filter']) && $_GET['user_level_filter'] == "Administrator" ? "selected" : ""); ?>>Administrator</option>
                    </select><br><br>
                    <input type="submit" name="btnSubmit" value="Submit" class="btn btn-success"/>
                    &nbsp;&nbsp;<input type="submit" name="btnCancel" value="Cancel" class="btn btn-danger"/>                    
                </div>                
            </div>
        </form>
        <?php if (isset($reportData)) 
        { ?>
              <div class="table-responsive">
                <table class="table table-hover">
                <thead>
                    <tr>
                        <?php foreach ($columnsToDisplay as $description => $column)
                           { ?>
                        <th><?php echo $description; ?></th>
                        <?php } ?>
                        <th colspan="2"></th>

                   </tr>
                </thead>
                <?php while($row = $reportData->fetch(PDO::FETCH_ASSOC)) 
                { ?>
                            <tr>
                                <?php foreach ($row as $column => $data)
                                {?>
                                    <td><?php echo $data; ?></td>                    
                                <?php } ?>
                            </tr>
                <?php } ?>                                       
                </table>
            </div>
        <div>
            <?php if (isset($_GET['page']) && $_GET['page'] > 1)
            { ?>
                <a class="btn btn-info" href="users_report.php?username_filter=<?php echo $_GET['username_filter']; ?>&user_level_filter=<?php echo $_GET['user_level_filter']; ?>&btnSubmit=Submit&page=<?php echo (isset($_GET['page']) ? $_GET['page']-1 : 1); ?>">Previous</a>
            <?php } ?>
            
            <a class="btn btn-info" href="users_report.php?username_filter=<?php echo $_GET['username_filter']; ?>&user_level_filter=<?php echo $_GET['user_level_filter']; ?>&btnSubmit=Submit&page=<?php echo (isset($_GET['page']) ? $_GET['page']+1 : 1); ?>">Next</a>
            
            <br><br>
            <a class="btn btn-warning" href="users_report.php?download=yes&username_filter=<?php echo $_GET['username_filter']; ?>&user_level_filter=<?php echo $_GET['user_level_filter']; ?>&btnSubmit=Submit">Download</a>
        </div>
        <?php } ?>
        </div>                
    </body>
</html>