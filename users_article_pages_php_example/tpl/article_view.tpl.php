<html>
    <head>
       <title>Article View</title>
       
       <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">

        <!-- jQuery library -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>

        <!-- Latest compiled JavaScript -->
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
   </head>
    <body>
            title: <?php echo (isset($dataValues['article_title']) ? $dataValues['article_title'] : ""); ?><br>
            date: <?php echo (isset($dataValues['article_date']) ? $dataValues['article_date'] : ""); ?><br>
            author: <?php echo (isset($dataValues['article_author']) ? $dataValues['article_author'] : ""); ?><br>
            content: <?php echo (isset($dataValues['article_content']) ? $dataValues['article_content'] : ""); ?><br>
            <a href="article_list.php">Back to List</a>
    </body>
</html>