<div>
  <div class="col-sm-3 col-md-offset-1">
       <h2>Current Weather</h2>
        <ul class="list">
            <li>Humidity: <?php echo $weatherWidget->weather->curren_weather[0]->humidity; ?></li>
            <li>Temperature: <?php echo $weatherWidget->weather->curren_weather[0]->temp; echo $weatherWidget->weather->curren_weather[0]->temp_unit; ?></li>
            <li><?php echo $weatherWidget->weather->curren_weather[0]->weather_text; ?></li>
            <li>Wind:<?php echo $weatherWidget->weather->curren_weather[0]->wind[0]->dir;?> at <?php echo $weatherWidget->weather->curren_weather[0]->wind[0]->speed; echo $weatherWidget->weather->curren_weather[0]->wind[0]->wind_unit;?></li>
            <li>Pressure: <?php echo $weatherWidget->weather->curren_weather[0]->pressure; ?></li>
        </ul>
    </div>
    
    
    <div class="col-sm-3 col-md-offset-1">
        <h2>Forecast Weather for <?php echo $weatherWidget->weather->forecast[0]->date; ?></h2>
            <h4>Day</h4>
           <ul class="list">
                <li><?php echo $weatherWidget->weather->forecast[0]->day[0]->weather_text; ?></li>
                <li>Wind:<?php echo $weatherWidget->weather->forecast[0]->day[0]->wind[0]->dir;?> at <?php echo $weatherWidget->weather->forecast[0]->day[0]->wind[0]->speed; echo $weatherWidget->weather->forecast[0]->day[0]->wind[0]->wind_unit;?></li>
                <li>Max-temp: <?php echo $weatherWidget->weather->forecast[0]->day_max_temp; echo $weatherWidget->weather->curren_weather[0]->temp_unit;?></li>
            </ul>

            <h4>Night</h4>
           <ul class="list">
                <li><?php echo $weatherWidget->weather->forecast[0]->night[0]->weather_text; ?></li>
                <li>Wind:<?php echo $weatherWidget->weather->forecast[0]->night[0]->wind[0]->dir;?> at <?php echo $weatherWidget->weather->forecast[0]->night[0]->wind[0]->speed; echo $weatherWidget->weather->forecast[0]->night[0]->wind[0]->wind_unit;?></li>
                <li>Min-temp: <?php echo $weatherWidget->weather->forecast[0]->night_min_temp; echo $weatherWidget->weather->curren_weather[0]->temp_unit;?></li>
            </ul>
        </div>
        
        
        
        <div class="col-sm-3 col-md-offset-1">
            <h2>Forecast Weather for <?php echo $weatherWidget->weather->forecast[1]->date; ?></h2>
            <h4>Day</h4>
               <ul class="list">
                    <li><?php echo $weatherWidget->weather->forecast[1]->day[0]->weather_text; ?></li>
                    <li>Wind:<?php echo $weatherWidget->weather->forecast[1]->day[0]->wind[0]->dir;?> at <?php echo $weatherWidget->weather->forecast[1]->day[0]->wind[0]->speed; echo $weatherWidget->weather->forecast[1]->day[0]->wind[0]->wind_unit;?></li>
                    <li>Max-temp: <?php echo $weatherWidget->weather->forecast[1]->day_max_temp; echo $weatherWidget->weather->curren_weather[0]->temp_unit;?></li>
                </ul>

            <h4>Night</h4>
               <ul class="list">
                    <li><?php echo $weatherWidget->weather->forecast[1]->night[0]->weather_text; ?></li>
                    <li>Wind:<?php echo $weatherWidget->weather->forecast[1]->night[0]->wind[0]->dir;?> at <?php echo $weatherWidget->weather->forecast[1]->night[0]->wind[0]->speed; echo $weatherWidget->weather->forecast[1]->night[0]->wind[0]->wind_unit;?></li>
                    <li>Min-temp: <?php echo $weatherWidget->weather->forecast[1]->night_min_temp; echo $weatherWidget->weather->curren_weather[0]->temp_unit;?></li>
                </ul>
        </div>
</div>