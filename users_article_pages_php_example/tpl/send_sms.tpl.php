<html>
    <head>
       <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">

        <!-- jQuery library -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>

        <!-- Latest compiled JavaScript -->
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
       <title>Send SMS Message</title>
       
        
    </head>
    <body>
        
        
        <div class="col-sm-6 col-sm-offset-3">
           
           <?php var_dump($sms->errors); ?>
           
            <form action="send_sms.php" method="post">
                <input type="hidden" name="honey_pot" value="<?php echo (isset($dataValues['honey_pot']) ? $dataValues['honey_pot'] : ""); ?>"/><br><br>
                
                Phone: <input type="text" name="phone" id="phone" value="<?php echo (isset($dataValues['phone']) ? $dataValues['phone'] : ""); ?>"/><br><br>

                
                Select Carrier of recipient:
                <select name="carrier" id="carrier">
                    <option value="">Please select one</option>

                    <option  value="vtext.com" <?php echo (isset($dataValues['carrier']) && $dataValues['carrier'] == "vtext.com" ? "selected" : ""); ?>>Verizon</option>

                    <option  value="vmobl.com" <?php echo (isset($dataValues['carrier']) && $dataValues['carrier'] == "vmobl.com" ? "selected" : ""); ?>>Virgin Mobile</option>

                    <option value="sms.alltelwireless.com" <?php echo (isset($dataValues['carrier']) && $dataValues['carrier'] == "sms.alltelwireless.com" ? "selected" : ""); ?>>Alltel</option>

                    <option value="txt.att.net" <?php echo (isset($dataValues['carrier']) && $dataValues['carrier'] == "txt.att.net" ? "selected" : ""); ?>>AT&amp;T</option>

                    <option value="sms.myboostmobile.com" <?php echo (isset($dataValues['carrier']) && $dataValues['carrier'] == "sms.myboostmobile.com" ? "selected" : ""); ?>>Boost Mobile</option>

                    <option value="text.republicwireless.com" <?php echo (isset($dataValues['carrier']) && $dataValues['carrier'] == "text.republicwireless.com" ? "selected" : ""); ?>>Republic Wireless</option>

                    <option value="messaging.sprintpcs.com" <?php echo (isset($dataValues['carrier']) && $dataValues['carrier'] == "messaging.sprintpcs.com" ? "selected" : ""); ?>>Sprint</option>

                    <option value="tmomail.net" <?php echo (isset($dataValues['carrier']) && $dataValues['carrier'] == "tmomail.net" ? "selected" : ""); ?>>T-Mobile</option>

                    <option value="email.uscc.net" <?php echo (isset($dataValues['carrier']) && $dataValues['carrier'] == "email.uscc.net" ? "selected" : ""); ?>>U.S. Cellular</option>

                </select><br><br>

                Message: <textarea name="message"><?php echo (isset($dataValues['message']) ? $dataValues['message'] : ""); ?></textarea><br><br>

                <input type="submit" name="btnSubmit" value="Submit"/><input type="submit" name="btnCancel" value="Cancel"/>


            </form>
        </div>  
               
    </body>
</html>