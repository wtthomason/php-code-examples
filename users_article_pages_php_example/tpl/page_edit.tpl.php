<html>
   <head>
       <title>Page Edit</title>
       
       <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">

        <!-- jQuery library -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>

        <!-- Latest compiled JavaScript -->
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
        
        <!--Tiny MCE include-->
        <script src='//cdn.tinymce.com/4/tinymce.min.js'></script>
         <!--Initialize Tiny MCE-->
          <script>
          tinymce.init({
            selector: '#content'
          });
          </script>
       
        <style>
            .error{
                color:red;
                font-style: oblique;
            }
        </style>
   </head>
    <body>
       <div class ="container">
           
            <div class="col-sm-4 col-sm-offset-4">
                <ul>
                <?php foreach ($pages->errors as $errorMsg) 
                { ?>
                    <li class="error"><?php echo $errorMsg; ?></li>
                <?php } ?>
                </ul>            
            </div>
          
           <div class="col-lg-6 col-sm-offset-4" >
                <form action="page_edit.php" method="post" enctype="multipart/form-data">
                  
                    <input type="hidden" name="page_id" value="<?php echo (isset($dataValues['page_id']) ? $dataValues['page_id'] : ""); ?>"/><br><br>

                    page title: <input type="text" name="page_title" value="<?php echo (isset($dataValues['page_title']) ? $dataValues['page_title'] : ""); ?>"/><br><br>

                    H1: <input type="text" name="h1" value="<?php echo (isset($dataValues['h1']) ? $dataValues['h1'] : ""); ?>"/><br><br>

                    URL Key:<input type="text" name="url_key" value="<?php echo (isset($dataValues['url_key']) ? $dataValues['url_key'] : ""); ?>"/><br><br>
                    
                    Meta Tags: <input type="text" name="meta_tags" value="<?php echo (isset($dataValues['meta_tags']) ? $dataValues['meta_tags'] : ""); ?>"/><br><br>

                    Content <textarea name="content" id="content" rows="15" cols="100"><?php echo (isset($dataValues['content']) ? $dataValues['content'] : ""); ?></textarea><br><br><br>
                    
                    Banner Image: <input type="file" id="banner_image" name="banner_image" value="<?php echo (isset($dataValues['banner_image']) ? $dataValues['banner_image'] : ""); ?>"/><br><br>              

                    <input type="submit" name="btnSubmit" value="Submit"/><input type="submit" name="btnCancel" value="Cancel"/>
                </form>
           </div>
        </div>
    </body>
</html>