<html>
   <head>
      <title>Page List</title>
       
       <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">

        <!-- jQuery library -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>

        <!-- Latest compiled JavaScript -->
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
       <script>
            function update(recId) //recId value passed through the input button
            {
                var userResponse1 = confirm("Are you sure you want to update this page?");	
                
                if(userResponse1)
                {
                    location.href = 'page_edit.php?page_id=' + recId; //goes to the update page with recId
                }

            }
           
           function view(recId) //recId value passed through the input button
            {
                var userResponse1 = confirm("Are you sure you want to view this page?");	
                alert("Inside View");
                if(userResponse1)
                {
                    location.href = 'page_view.php?page=' + recId; //goes to the update page with recId
                }

            }
           
           
       </script>
   </head>
    <body>
        
        

        <div class="container">
        <h1>The following Pages were Found.</h1>
        <a href="page_edit.php">Creat a new Page</a>
        <br><a href ="users_list.php">User List</a>
        <form action="article_list.php" method="get">
            Search: <input name="search" value="<?php echo $searchValue; ?>"/>
            <input type="submit" name="btnSearch" value="Search"/>
        </form>
        
        <div class="table-responsive">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <?php foreach ($columnsToDisplay as $description => $column)
                        { ?>
                            <th><?php echo $description; ?></th>
                        <?php } ?>
                            <th colspan="2"></th>

                    </tr>
                </thead>
                <?php while($row = $pageList->fetch(PDO::FETCH_ASSOC)) 
                    { ?>
                        <tr>
                            <?php foreach ($columnsToDisplay as $description => $column)
                            {?>
                                <td><?php echo $row[$column]; ?></td>                    
                            <?php } ?>
                            <td>
                                <!--<button type="button" value="View"><a href="page_view.php?page=<?php //echo $row['url_key']; ?>"><span class="glyphicon glyphicon-eye-open"></span></a></button>-->
                                <a href="page_view.php?page=<?php echo $row['url_key']; ?>"><span class="glyphicon glyphicon-eye-open"></span></a>
                            </td>
                            <td>
                                <button type="button"  value="Update" onclick="update(<?php echo $row['page_id']; ?>)"><span class="glyphicon glyphicon-pencil"></span></button>
                            </td>
                        </tr>
            <?php   } ?>                        
            </table>
            </div>
        </div>
        
    </body>
</html>