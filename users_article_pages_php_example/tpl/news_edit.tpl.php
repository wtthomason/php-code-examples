<html>
   <head>
       <title>News Edit</title>
       
       <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">

        <!-- jQuery library -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>

        <!-- Latest compiled JavaScript -->
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
   </head>
    <body>
        
        <div>
            <ul>
            <?php foreach ($newsArticle->errors as $errorMsg) 
            { ?>
                <li class="test"><?php echo $errorMsg; ?></li>
            <?php } ?>
            </ul>            
        </div>
        
        <form action="news_edit.php" method="post">
            <input type="hidden" name="article_id" value="<?php echo (isset($dataValues['article_id']) ? $dataValues['article_id'] : ""); ?>"/><br><br>
            title: <input type="text" name="article_title" value="<?php echo (isset($dataValues['article_title']) ? $dataValues['article_title'] : ""); ?>"/><br><br>
            date: <input type="text" name="article_date" value="<?php echo (isset($dataValues['article_date']) ? $dataValues['article_date'] : ""); ?>"/><br><br>
            author: <input type="text" name="article_author" value="<?php echo (isset($dataValues['article_author']) ? $dataValues['article_author'] : ""); ?>"/><br><br>
            content: <textarea name="article_content"><?php echo (isset($dataValues['article_content']) ? $dataValues['article_content'] : ""); ?></textarea><br><br>
            <input type="submit" name="btnSubmit" value="Submit"/>
        </form>    
    </body>
</html>