
 <html>
   <head>
       <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">

        <!-- jQuery library -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>

        <!-- Latest compiled JavaScript -->
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
        
         <script>
            function update(recId) //recId value passed through the input button
            {
                var userResponse1 = confirm("Are you sure you want to update this User?");	
                
                if(userResponse1)
                {
                    location.href = 'users_edit.php?user_id=' + recId; //goes to the update page with recId
                }

            }
           
           function view(recId) //recId value passed through the input button
            {
                var userResponse1 = confirm("Are you sure you want to view this User?");	
                
                if(userResponse1)
                {
                    location.href = 'users_view.php?user_id=' + recId; //goes to the update page with recId
                }

            }
           
           
       </script>
   </head>
    <body>
       <?php include ("curl_weather.php");?>
        <div class="container">
             <a href ="user_login.php"><span class="glyphicon glyphicon-user"></span>Login Here</a>
             <br><a href ="users_edit.php"><span class="glyphicon glyphicon-plus-sign"></span>Create New User</a>
             <br><a href ="users_report.php"><span class="glyphicon glyphicon-list-alt"></span>Create User Report</a>
             <br><a href ="rest_get_users.php">Rest User list</a>
             <br><a href ="article_list.php">Article List</a>
             <br><a href ="page_list.php">Page List</a>
             <br><a href ="send_sms.php">Send SMS Message</a>
             
             
             
            <form action="users_list.php" method="get">
                Search: <input name="search" value="<?php echo $searchValue; ?>"/>
                <span class="glyphicon glyphicon-search"></span><input type="submit" name="btnSearch" value="Search"/>
            </form>

 
            <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <?php foreach ($columnsToDisplay as $description => $column)
                                { ?>
                                    <th><?php echo $description; ?></th>
                                <?php } ?>
                                    <th colspan="2"></th>

                            </tr>
                        </thead>
                        <?php while($row = $users->fetch(PDO::FETCH_ASSOC)) 
                        { ?>
                                    <tr>
                                        <?php foreach ($columnsToDisplay as $description => $column)
                                        {?>
                                            <td><?php echo $row[$column]; ?></td>                    
                                        <?php } ?>
                                        <td>
                                            <button type="button" value="View" onclick="view(<?php echo $row['user_id']; ?>)"><span class="glyphicon glyphicon-eye-open"></span></button>
                                        </td>
                                        <td>
                                            <button type="button"  value="Update" onclick="update(<?php echo $row['user_id']; ?>)"><span class="glyphicon glyphicon-pencil"></span></button>
                                        </td>
                                    </tr>
                        <?php } ?>            
                    </table>
                    </div>
        </div>
    </body>
</html>		