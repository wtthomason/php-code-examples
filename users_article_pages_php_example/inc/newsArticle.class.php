<?php
require_once('base.class.php');

class newsArticle extends base 
{
    var $tableName = 'news_articles';
    var $keyField = 'article_id';   
    var $rows = array();
    
    function sanitize($sanitizeData) 
    {
        $sanitizeData['article_title'] = filter_var($sanitizeData['article_title'], FILTER_SANITIZE_STRING);
        $sanitizeData['article_date'] = filter_var($sanitizeData['article_date'], FILTER_SANITIZE_STRING);
        $sanitizeData['article_author'] = filter_var($sanitizeData['article_author'], FILTER_SANITIZE_STRING);
        $sanitizeData['article_content'] = filter_var($sanitizeData['article_content'], FILTER_SANITIZE_STRING);
        

        return $sanitizeData; 
    }
    
    function validate()
    {
        $success = true;
    
        if (empty($this->data['article_title'])) 
        {
            $this->errors['article_title'] = 'Please enter a title';
            $success = false;
        }
        
        if(strlen($this->data['article_title']) > 150)
        {
            $this->errors['article_title'] = 'Title is to long. 150 charector limit';
            $success = false;
        }

        if (empty($this->data['article_date'])) 
        {
            $this->errors['article_date'] = 'Please enter a date';
            $success = false;
        }
        
        if (strlen($this->data['article_date'])<10)
        {
        $success = false;
        $this->errors['article_date'] = "invalid Date please use yyyy/mm/dd.";
        }

        if (strlen($this->data['article_date'])>10)
        {
        $success = false;
        $this->errors['article_date'] = "invalid Date please use yyyy/mm/dd.";
        }
        
        if (empty($this->data['article_author'])) 
        {
            $this->errors['article_author'] = 'Please enter an auther name';
            $success = false;
        }
        
        if(strlen($this->data['article_author']) > 50)
        {
            $this->errors['article_author'] = 'Author name is to long. 50 charector limit';
            $success = false;
        }

        if (empty($this->data['article_content'])) 
        {
            $this->errors['article_content'] = 'Please enter some content';
            $success = false;
        }

        

        return $success;

    }
    
    function getArticles($searchValue = null)
    {
        
        $listSQL = "SELECT * FROM " . $this->tableName;
        $parameterList = array();
        
        if (!is_null($searchValue) && !empty($searchValue))
        {
            $listSQL .= " WHERE article_content LIKE ? OR article_author LIKE ? OR article_title LIKE ?";
            $parameterList[] = '%' . $searchValue . '%';
            $parameterList[] = '%' . $searchValue . '%';
            $parameterList[] =  '%' . $searchValue . '%';
        }
        
        $stmt = $this->db->prepare($listSQL);
        $stmt->execute($parameterList);
        
        return $stmt;
        
    }
}
