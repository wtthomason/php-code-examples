<?php
require_once('base.class.php');

class pages extends base 
{
    var $tableName = 'pages';
    var $keyField = 'page_id'; 
    var $searchableFields = array('page_title', 'meta_tags', 'url_key');
        
    function sanitize($sanitizeData) 
    {
        $sanitizeData['page_title'] = filter_var($sanitizeData['page_title'], FILTER_SANITIZE_STRING);
        
        $sanitizeData['h1'] = filter_var($sanitizeData['h1'], FILTER_SANITIZE_STRING);
        
        $sanitizeData['url_key'] = filter_var($sanitizeData['url_key'], FILTER_SANITIZE_STRING);
        
        $sanitizeData['meta_tags'] = filter_var($sanitizeData['meta_tags'], FILTER_SANITIZE_STRING);
        
        return $sanitizeData;
    }
    
    function validate($validateData)
    {
        $success = true;
        
        if (empty($this->data['page_title'])) 
        {
            $this->errors['page_title'] = 'Please enter a Page title.';
            $success = false;
        }
        
        if(strlen($this->data['page_title']) > 200)
        {
            $this->errors['page_title'] = 'Page title is to long. 200 charector limit.';
            $success = false;
        }
        
        if (empty($this->data['h1'])) 
        {
            $this->errors['h1'] = 'Please enter a H1.';
            $success = false;
        }
        
        if(strlen($this->data['h1']) > 200)
        {
            $this->errors['h1'] = 'H1 is to long. 200 charector limit.';
            $success = false;
        }
        
        if (empty($this->data['meta_tags'])) 
        {
            $this->errors['meta_tags'] = 'Please enter a Meta Tags.';
            $success = false;
        }
        
        if(strlen($this->data['meta_tags']) > 200)
        {
            $this->errors['meta_tags'] = 'Meta Tags are to long. 200 charector limit.';
            $success = false;
        }
        
        if (empty($this->data['content'])) 
        {
            $this->errors['content'] = 'Please enter content.';
            $success = false;
        }
        
        if (empty($this->data['url_key'])) 
        {
            $this->errors['url_key'] = 'Please enter a URl Key.';
            $success = false;
        }
        
        if(strlen($this->data['url_key']) > 200)
        {
            $this->errors['url_key'] = 'URL Key are to long. 200 charector limit.';
            $success = false;
        }
        
       
        
        return $success;
    }
    
    function loadByURLKey($url_key)
    {
        $success = false;
        
        $stmt = "SELECT * FROM " . $this->tableName . " WHERE url_key = ?";    
        
        $stmt = $this->db->prepare($stmt); 
        
        $stmt->execute(array($url_key));

        if ($stmt->rowCount() == 1) {
            $success = true;
            $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);        
            $this->data = $rows[0];
      
        }
        
        return $success;
    }
    
    function saveUploadedImage($fileArray)
    {
        if (isset($_FILES['banner_image']) && 
            isset($_FILES['banner_image']['tmp_name']) && 
            !empty($_FILES['banner_image']['tmp_name']))
        {
            move_uploaded_file($_FILES['banner_image']['tmp_name'], 
                    dirname(__FILE__) . "/../public_html/images/" . $this->data['url_key'] . "_page_banner.jpg");
        }        
    }
    
}
?>