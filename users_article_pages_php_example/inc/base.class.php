<?php
class base 
{
    var $data = array();
    var $db = null;
    var $errors = array();
    var $tableName = null;
    var $keyField = null;
    
        
    function __construct() 
    {
        $this->connectToDB();
    }
    
    function connectToDB() 
    {
        $success = true;
        
         $this->db = new PDO("mysql:host=localhost;dbname=databaseName;charset=utf8", "userName", "password"); //replace Database name, username & password with the one on your host account
        
        
        
        if (!$this->db)
        {
            $errors[] = 'Unable to connect to database';
            $success = false;
        }
        
        return $success;
    }
        
    function set($setData) 
    {
        $this->data = $this->sanitize($setData);
    }
    
    function sanitize($sanitizeData) 
    {        
        return $sanitizeData;
    }
    
    function validate($validateData)
    {
        $this->data = $this->sanatize($validateData);
        
        //return false;
    }
    
    function load($loadId)
    {
        $success = false;
        
        $stmt = $this->db->prepare("SELECT * FROM " . $this->tableName . " WHERE " . $this->keyField . " = ?");
        $stmt->execute(array($loadId));

        if ($stmt->rowCount() == 1) {
            $success = true;
            $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);        
            $this->data = $rows[0];
     
        }
        
        return $success;
    }
    
    function save()
    {
        $success = true;
        
        if (isset($this->data[$this->keyField]) && !empty($this->data[$this->keyField])) 
        {
            // update
            $updateSQL = "UPDATE " . $this->tableName . " SET ";
            
            $count = 0;
            foreach ($this->data as $columnName => $columnValue) 
            {
                $updateSQL .= ($count++ > 0 ? "," : "") . $columnName . " = ?";                
            }
            
            $updateSQL .= " WHERE " . $this->keyField . " = ?";
            
            $updateValues = array_values($this->data);
            $updateValues[] = $this->data[$this->keyField];
            
            //var_dump($updateSQL, $updateValues);
            
            $stmt = $this->db->prepare($updateSQL);
            if (!$stmt->execute($updateValues)) 
            {
                $success = false;
                $this->errors[] = $stmt->errorInfo();
            }
        } 
        else 
        {
            // insert
            $updateSQL = "INSERT INTO " . $this->tableName . " SET ";
            
            $count = 0;
            foreach ($this->data as $columnName => $columnValue) 
            {
                $updateSQL .= ($count++ > 0 ? "," : "") . $columnName . " = ?";                
            }
                        
            $updateValues = array_values($this->data);
            
            //var_dump($updateSQL, $updateValues);
            
            $stmt = $this->db->prepare($updateSQL);
            if (!$stmt->execute($updateValues)) 
            {
                $success = false;
                $this->errors[] = $stmt->errorInfo();
            }
            else 
            {
                $this->data[$this->keyField] = $this->db->lastInsertId();
            }
        }
                
        return $success;
    }
    
    
    
    function getList()
    {
        $listSQL = "SELECT * FROM " . $this->tableName;

        
        $stmt = $this->db->prepare($listSQL);
        $stmt->execute();
        
            
            
           return $stmt;
    }
    
      function getListArray() 
    {
        $stmt = $this->db->prepare("SELECT * FROM " . $this->tableName);
        $stmt->execute();
        
        return $stmt->fetchAll(PDO::FETCH_ASSOC);        
    }
    
    static function curlGet($url)
    {
        $curSession = curl_init();

        $result = null;

        if ($curSession) 
        {
            curl_setopt($curSession, CURLOPT_URL, $url);
            curl_setopt($curSession, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curSession, CURLOPT_FOLLOWLOCATION, true);

            $result = curl_exec($curSession);

            curl_close($curSession);    
        }
        
        return $result;
    }
}



?>
