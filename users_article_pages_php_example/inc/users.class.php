<?php
require_once('base.class.php');

class users extends base 
{
    var $tableName = 'users';
    var $keyField = 'user_id';   
    var $reportResultsPerPage = 5;
    function sanitize($sanitizeData) 
    {
        $sanitizeData['username'] = filter_var($sanitizeData['username'], FILTER_SANITIZE_STRING);
        $sanitizeData['password'] = filter_var($sanitizeData['password'], FILTER_SANITIZE_STRING);
        $sanitizeData['user_level'] = filter_var($sanitizeData['user_level'], FILTER_SANITIZE_STRING);
        
        

        return $sanitizeData;
    }
    
    function validate($sanitizeData)
    {
        $success = true;
    
        if (empty($this->data['username'])) 
        {
            $this->errors['username'] = 'Please enter a username';
            $success = false;
        }
            elseif(strlen($this->data['username']) > 15)
            {
               $this->errors['username'] ='User name is to long. Must be less than 15 characters'; 
               $success = false;    
            }
        if (empty($this->data['password'])) 
        {
            $this->errors['passwrod'] = 'Please enter a password';
            $success = false;
        } 
        
         elseif(strlen($this->data['password']) > 10)
            {
               $this->errors['article_date'] ='Password cant be more than 10 characters long'; 
               $success = false;    
            }
        

        if (empty($this->data['user_level'])) 
        {
            $this->errors['user_level'] = 'Please select a user level';
            $success = false;
        }
        
       
        return $success;

    }
    

     
        
     function getList($searchValue = null) 
    {
        $listSQL = "SELECT * FROM " . $this->tableName;
        $parameterList = array();
        
        if (!is_null($searchValue) && !empty($searchValue))
        {
            $listSQL .= " WHERE username LIKE ? OR password LIKE ? OR user_level LIKE ?";
            $parameterList[] = '%' . $searchValue . '%';
            $parameterList[] = '%' . $searchValue . '%';
            $parameterList[] =  '%' . $searchValue . '%';
        }
        
        $stmt = $this->db->prepare($listSQL);
        $stmt->execute($parameterList);
        
        return $stmt;
    }
    
    function login($userInfo)
    {
        $success = false;
        
        $loginSQL = "SELECT user_id FROM " . $this->tableName . " WHERE username = :username AND password = :password";        
        // need to call prepare
        $stmt = $this->db->prepare($loginSQL);        
        // need to call execute
        $stmt->execute($userInfo);
        // store the user_id to session
        
        if ($stmt->rowCount() == 1){
            
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
            if ($row['user_id'] > 0)
            {
                $success = true;
                $_SESSION['user_id'] = $row['user_id'];
                var_dump($_SESSION, session_id());
            }
        }
        return $success;
    }
    
     function saveUploadedImage($fileArray)
     {
         
         
        if (isset($_FILES['user_image']) &&
            isset($_FILES['user_image']['tmp_name']) && 
            !empty($_FILES['user_image']['tmp_name']))
         {   
         
         
             $errors =array();
            
             $file_size =$_FILES['user_image']['size'];
             $file_type =$_FILES['user_image']['type'];
             $file_ext=strtolower(end(explode('.',$_FILES['user_image']['name'])));
            
             $extensions= array("jpeg","jpg","png");
             
             if(in_array($file_ext,$extensions)===false)
             {
                 $errors[]="extension not allowed, please choose a JPEG file.";
                 
             }
             
             if($file_size > 2097153)
             {
                 $errors[]= 'File size must be exactly 2 MB';
             }
         
         
        if(empty($errors)== true) 
        {
            move_uploaded_file($_FILES['user_image']['tmp_name'], 
                    dirname(__FILE__) . "/../public_html/images/" . $this->data['user_id'] . "_user.jpg");
            echo "Success";
        } 
           else
        {
            return $errors;
        }        
      }
    
    }//end of saveUploadedImage
    
    function checkUserLevel($user_id)
    {   
        $loginSQL = "SELECT user_level FROM " . $this->tableName . " WHERE user_id =" . $user_id;     
        // need to call prepare
        $stmt = $this->db->prepare($loginSQL);        
        // need to call execute
        $stmt->execute(array($user_id));
     
        $user_level = $stmt->fetch(PDO::FETCH_ASSOC);;
        
        
        if($user_level['user_level'] != "Administrator")
        {
            
            exit(header("Location: users_list.php"));
        }
    }
    
    function getUserReportData($reportFilters) 
    {
        $listSQL = "SELECT username , user_level FROM " . $this->tableName;
        $parameterList = array();
        $stmt = null;
        
        if (!empty($reportFilters) && is_array($reportFilters))
        {
            $filterPassed = false;
            
            $listSQL .= " WHERE ";
            
            if (isset($reportFilters['username_filter']) && !empty($reportFilters['username_filter'])) 
            {
                $listSQL .= "username LIKE ?";                                
                $parameterList[] = '%' . $reportFilters['username_filter'] . '%';
                $filterPassed = true;
            }            

            if (
                (isset($reportFilters['user_level_filter']) && !empty($reportFilters['user_level_filter']))
               )
            {
                $listSQL .= ($filterPassed ? " AND " : "") . 
                    "user_level BETWEEN ? AND ?";                                
                $parameterList[] = $reportFilters['user_level_filter'];
                $parameterList[] = $reportFilters['user_level_filter'];
                $filterPassed = true;
            }            
            
            if (isset($reportFilters['page']) && !empty($reportFilters['page'])) 
            {
                $listSQL .= " LIMIT " . ($this->reportResultsPerPage * ($reportFilters['page'] - 1)) . "," . $this->reportResultsPerPage;
            }            
            
            if ($filterPassed)
            {
                $stmt = $this->db->prepare($listSQL);
                $stmt->execute($parameterList);            
            }
        }
              
        return $stmt;
    }
    
    public function downloadUserReport($reportFilters)
    {
        $reportData = $this->getUserReportData($reportFilters);
        
        if (isset($reportData))
        {
            $timestamp = date("YmdHis");
            
            header("Content-Type: text/csv");
            header('Content-Disposition: attachment; filename="' . $timestamp . '_User_Report.csv"');
            while($row = $reportData->fetch(PDO::FETCH_ASSOC)) 
            {
                echo implode(",", $row) . "\r\n";
            }
        }        
    }    
}
