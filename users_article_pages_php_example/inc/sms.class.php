<?php
require_once('base.class.php');

class sms extends base 
{
    var $tableName = '';
    var $keyField = 'user_id';   
    var $reportResultsPerPage = 5;
    
    function sanitize($sanitizeData) 
    {
        $sanitizeData['phone'] = filter_var($sanitizeData['phone'], FILTER_SANITIZE_STRING);
        $sanitizeData['phone'] = str_replace("-","",$sanitizeData['phone']);
        $sanitizeData['message'] = filter_var($sanitizeData['message'], FILTER_SANITIZE_STRING);
        
        return $sanitizeData;
    }
    
    function validate($sanitizeData)
    {
        $success = true;
    
        if (empty($this->data['phone'])) 
        {
            $this->errors['phone'] = 'Please enter a phone number.';
            $success = false;
        }
            elseif(strlen($this->data['phone']) > 10)
            {
               $this->errors['phone'] ='Please enter a vailid phone number'; 
               $success = false;    
            }
          

        if (empty($this->data['message'])) 
        {
            $this->errors['message'] = 'Please enter a message.';
            $success = false;
        }
        
        if (empty($this->data['carrier'])) 
        {
            $this->errors['carrier'] = 'Please select a carrier.';
            $success = false;
        }
        
        if(!empty($this->data['honey_pot']))
        {
           $this->errors['honey_pot'] = 'You are a robot!!!!!';
           $success = false;
        }
        
       
        return $success;

    }
    
    function sendSms()
    {
        
        $to = $this->data['phone'] . "@" . $this->data['carrier'];
        
        $from = "william@williamthomason.info";
        
        $message = $this->data['message'];
        
        $headers = "From: $from/n";
       
        mail($to, '', $message, $headers);
            
        
        
        return true;
    }
        
}

?>